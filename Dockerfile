# Use the official Node.js 10 image.
# https://hub.docker.com/_/node
FROM ajardin/yarn

# default port on cloud run service https://cloud.google.com/run/docs/reference/container-contract
ARG port=8080

# installe un simple serveur http pour servir un contenu statique
RUN yarn global add http-server


# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY package.json package*.json ./

# installe les dépendances du projet
RUN yarn

# Copy local code to the container image.
COPY . .

# construit l'app pour la production en la minifiant
RUN yarn build

EXPOSE $port


CMD [ "http-server", "dist" ]