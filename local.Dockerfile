# Use the official Node.js 10 image.
# https://hub.docker.com/_/node
FROM ajardin/yarn

# default port on cloud run service https://cloud.google.com/run/docs/reference/container-contract
ARG port=8080

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY package.json package*.json ./

# # Install production dependencies.
# RUN npm install --only=production

# installe les dépendances du projet
RUN yarn

# Copy local code to the container image.
COPY . .

EXPOSE $port

CMD [ "yarn", "serve" ]